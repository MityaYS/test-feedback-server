alembic==1.7.7
aniso8601==9.0.1
blinker==1.4
click==8.0.4
colorama==0.4.4
Flask==2.0.3
Flask-Cors==3.0.10
Flask-JWT-Extended==4.3.1
Flask-Mail==0.9.1
flask-marshmallow==0.14.0
Flask-Migrate==3.1.0
Flask-RESTful==0.3.9
Flask-Script==2.0.6
Flask-SQLAlchemy==2.5.1
greenlet==1.1.2
importlib-metadata==4.11.3
importlib-resources==5.4.0
itsdangerous==2.1.1
Jinja2==3.0.3
Mako==1.2.0
MarkupSafe==2.1.1
marshmallow==3.15.0
marshmallow-sqlalchemy==0.28.0
packaging==21.3
psycopg2==2.9.3
PyJWT==2.3.0
pyparsing==3.0.7
pytz==2022.1
six==1.16.0
SQLAlchemy==1.4.32
typing-extensions==4.1.1
Werkzeug==2.0.3
zipp==3.7.0
