from datetime import date, datetime, timedelta
import json

from pytz import timezone
from common.mail_utils import sendStandartMessage
from models.feedback import Feedback
from models.user import User
from flask_restful import Resource, abort, request
from flask import g, jsonify, make_response, render_template_string
from common.auth_utils import clientAuthCheck
from config import hoursLimitForRequest, dirFeedbackFiles, backendAddress
from settings import db
from schemas.feedback import FeedbackSchema
from werkzeug.utils import secure_filename

class FeedbackClientRoute(Resource):
    @clientAuthCheck(required=True)
    def get(self):
        pass
    
    @clientAuthCheck(required=True)
    def put(self):
        g.user.last_date_request = None
        db.session.commit()
        return True
    
    @clientAuthCheck(required=True)
    def post(self):
        data = request.json
        data = json.loads(request.files['json'].read())
        
        subjectValidate = len(data.get('subject')) >= 6 and len(data.get('subject')) <= 120    
        messageValidate = len(data.get('message')) >= 10 and len(data.get('message')) <= 500
        timeLastRequest = (g.user.last_date_request + timedelta(hours=hoursLimitForRequest)).timestamp() if g.user.last_date_request else None
        userLimits = not timeLastRequest or (timeLastRequest and  timeLastRequest <= datetime.now().timestamp())
        
        if (not userLimits):
            remainingTime = timeLastRequest - datetime.now().timestamp()
            return make_response(jsonify({"type": "limitForTimeRequest", "value": remainingTime}), 400)

        
        if (subjectValidate and messageValidate):
            feedback = Feedback(
                subject=data['subject'],
                message=data['message'],
                date_create=datetime.now(),
                id_user=g.user.id,
                file_link= None
            )
            
            db.session.add(feedback)
            g.user.last_date_request = feedback.date_create
            db.session.commit()
            
            link = None
            if (request.files.get('file')):
                file_type = ''
                if (len(request.files['file'].filename.split('.')) > 1):
                    file_type = '.' + request.files['file'].filename.split('.')[-1]
                request.files['file'].save(dirFeedbackFiles + secure_filename('feedback_{identity}{file_type}'.format(identity=feedback.id, file_type=file_type)))
                link = '/' + dirFeedbackFiles + 'feedback_{identity}{file_type}'.format(identity=feedback.id, file_type=file_type)    
            
            feedback.file_link = link
            db.session.commit()
            
            sendMessageForAdmin(feedback)

            return 200

    @clientAuthCheck(required=True)
    def delete(self):
        pass
    

def sendMessageForAdmin(feedback):
    
    feedback_data = FeedbackSchema(only=[
        'id', 'subject', 'message', 'file_link', 'user.id', 'user.name', 'user.email'
    ]).dump(feedback)
    recipients = [user.email for user in User.query.filter(User.role == 'admin').all()]
    
    sendStandartMessage(
        "[Feedback] " + feedback_data['subject'],
        render_template_string('''
            <div style="font-size: 18px">
            <div>User name: <b>{{ feedback_data['user']['name'] }}<b></div>
            <div>User email: {{ feedback_data['user']['email'] }}</div>
            {% if feedback_data['file_link'] %}
            <div>File link: {{ backendAddress + feedback_data['file_link'] }}</div>
            {% endif %}
            <hr>
            <h3>{{ feedback_data['subject'] }}</h3>
            <span>{{ feedback_data['message'] }}</span>
            </div>
        ''', feedback_data=feedback_data, backendAddress=backendAddress),
        recipients
    )