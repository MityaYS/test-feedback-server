from flask import jsonify, make_response
from flask_restful import Resource, abort, request
from common.auth_utils import adminAuthCheck
from settings import db

from models.feedback import Feedback
from schemas.feedback import FeedbackSchema

class FeedbackAdminRoute(Resource):
    @adminAuthCheck()
    def get(self):
        per_page = 11
        page = int(request.args.get('page', 1))
        feedbacks = Feedback.query.order_by(Feedback.date_create.desc()).paginate(page, per_page)
        
        return make_response(jsonify({
            "feedbacks": FeedbackSchema(many=True).dump(feedbacks.items),
            "total_pages": feedbacks.pages
            }))
    
    @adminAuthCheck()
    def put(self):
        feedback = Feedback.query.get(request.json.get('id'))
        if feedback:
            feedback.is_viewed = request.json.get('is_viewed')
            db.session.commit()
            
            return FeedbackSchema(only=['id', 'is_viewed']).dump(feedback)
    
    @adminAuthCheck()
    def post(self):
        pass
    
    @adminAuthCheck()
    def delete(self):
        pass