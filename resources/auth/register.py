from models.user import User
from settings import db
from common.mail_utils import sendStandartMessage
from datetime import datetime
from config import backendAddress
from flask import jsonify, render_template_string, request, make_response, redirect, g
from werkzeug.security import generate_password_hash
from config import frontendAddress
from common.auth_utils import createToken

def sendRegisterMessage(user):
    
    msgLink = "{}/register-step-2?id={}&key={}".format(backendAddress, str(user.id), user.registerData["key"])
    sendStandartMessage(
        "Confirm your email address on Test Feedback",
        render_template_string('''
            <p>{{"Please click the button below to confirm your email address and complete registration"}}:</p>
            <a href="{{msgLink}}" style="background-color: #ff3b3f;padding: 15px 30px;text-decoration: none;color: white;border-radius: 5px;font-size: 16px;display: block;max-width: 200px;margin: 10px auto 0;">Confirm</a>
        ''', msgLink=msgLink),
        [user.email]
    )
    return True


# Send register email
def clientRegisterStep1():
    
    if (
        len(request.json["email"]) >= 128 or
        len(request.json["password"]) >= 128 or
        len(request.json["name"]) >= 128
        ):
        return jsonify({"type": "unknownError"}), 400

    if User.query.filter_by(email=request.json["email"]).count() > 0:
        return jsonify({"type": "emailIsBusy", "field": "email"}), 400

    user = User(
        email=request.json["email"],
        name=request.json["name"],
        passHash=generate_password_hash(request.json["password"])
    )
    db.session.add(user)
    db.session.commit()
    sendRegisterMessage(user)
    return ""

# Route for mail confirm message
def clientRegisterStep2():
    user = User.query.get(int(request.args.get("id")))
    g.user = None

    if (not user or user.registerData["isActive"] or
            request.full_path != "/register-step-2?id={}&key={}".format(str(user.id), user.registerData["key"])):
        return make_response(redirect(frontendAddress + "/message/error-auth"))

    if g.user:
        g.user.token = None

    g.user = user
    user.registerData = {"isActive": True, "key": None}
    user.date_register = datetime.now()
    user.token = createToken(user.id, "client")
    db.session.commit()

    return make_response(redirect(frontendAddress + "/message/success-auth"))
