
from flask import jsonify, request, g
from settings import db
from werkzeug.security import check_password_hash
from common.auth_utils import clientAuthCheck, createToken, _getUser, getUserData
from models.user import User
from resources.auth.register import sendRegisterMessage


@clientAuthCheck()
def clientLogin():
    user = User.query.filter(User.email == request.json["email"]).first()
    if not user or not check_password_hash(user.passHash, request.json["password"]):
        return jsonify({"type": "invalidEmailOrPassword"}), 400
    elif not user.registerData["isActive"]:
        if sendRegisterMessage(user):
            return jsonify({"type": "userNotIsActive"}), 401
        else:
            return jsonify({"type": "messagesLimit"}), 401
    else:
        if g.user:
            g.user.token = None

        user.token = createToken(user.id, "client")
        db.session.commit()

        return jsonify(user=getUserData(user), token=user.token)
