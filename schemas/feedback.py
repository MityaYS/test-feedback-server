
from models.feedback import Feedback
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from settings import ma

from schemas.user import UserSchema

class FeedbackSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Feedback
        
    user = ma.Nested(UserSchema,
                     only=('id', 'name', 'email'))