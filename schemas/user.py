
from models.user import User
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from settings import ma

class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        
    feedbacks = ma.Nested('FeedbackSchema', many=True,
                           only=('id', 'subject', 'message', 'file_link'))