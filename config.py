from datetime import timedelta


postgres = {
    "user": "postgres",
    "password": "123",
    "host": "localhost",
    "db_name": "test_feedback"
}

class Config:
    PROPAGATE_EXCEPTIONS = True
    SECRET_KEY = "UddfvggDFGtfhgfygiyDFGt67tFgF6G8gf7ngf"

    SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{password}@{host}/{db_name}'.format(**postgres)
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=7)
    JWT_HEADER_TYPE = ""

    UPLOAD_FOLDER = 'static'

    CORS_EXPOSE_HEADERS = ["Navigate", "Show-Message",
                           "Authorization", "Language", "Set-Token"]
    CORS_RESOURCES = ["/static/*", "/*"]

    # GOOGLE TEST MAIL
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'test.acc31082021@gmail.com'
    MAIL_PASSWORD = 'testforfeedback'

backendAddress = "http://127.0.0.1:5000"
frontendAddress = "http://localhost:4200/"
hoursLimitForRequest = 24
dirFeedbackFiles = 'static/feedback_files/'

admins = [
    {
        "email": "romaha.andrei.ok@gmail.com",
        "name": "Andrei Romaha",
        "password": "12345"
    }
]