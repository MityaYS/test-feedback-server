from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
# from flask_script import Manager
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from flask_restful import Api
from flask_mail import Mail

app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)
ma = Marshmallow(app)
migrate = Migrate(app, db)
# manager = Manager(app)
# manager.add_command('db', MigrateCommand)
api = Api(app)
mail = Mail(app)
jwt = JWTManager(app)
CORS(app)

jwt = JWTManager(app)
CORS(app)