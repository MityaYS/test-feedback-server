from email.policy import default
from settings import db
from sqlalchemy.dialects.postgresql import JSONB
from uuid import uuid4
from settings import ma

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text)
    email = db.Column(db.Text)
    date_register = db.Column(db.DateTime(timezone=True), default=None)
    role = db.Column(db.Text, default="client") # client | admin
    
    passHash = db.Column(db.String(256), default="")
    token = db.Column(db.Text)
    registerData = db.Column(JSONB, default={"key": str(uuid4()), "isActive": False})
    last_date_request = db.Column(db.DateTime(timezone=True), default=None)
    

    
    feedbacks = ma.Nested('FeedbackSchema', many=True,
                           only=('id', 'subject', 'message', 'date_create', 'file_link'))
