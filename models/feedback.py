from settings import db
from sqlalchemy.dialects.postgresql import JSONB
from models.user import User

class Feedback(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    subject = db.Column(db.String(120))
    message = db.Column(db.String(500))
    file_link = db.Column(db.Text)
    date_create = db.Column(db.DateTime(timezone=True), default=None)
    is_viewed = db.Column(db.Boolean, default=False)
    
    id_user = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=True)
    user = db.relationship(
        User, backref=db.backref('feedbacks', lazy='dynamic'))
