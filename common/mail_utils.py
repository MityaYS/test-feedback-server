from settings import mail, app
from flask_mail import Message
from flask import render_template
from threading import Thread

def sendStandartMessage(title, body, recipients):
    msg_html = render_template(
        "messageBase.html",
        body=body
    )
    msg = Message(title, sender=app.config["MAIL_USERNAME"], recipients=recipients, html=msg_html)

    def _sendMsg():
        with app.app_context():
            mail.send(msg)
    Thread(target=_sendMsg).start()
