
import jwt
from settings import app, db
from datetime import datetime
from functools import wraps
from flask import abort, g, jsonify, request
from models.user import User
from schemas.user import UserSchema

tokenExpDays = 7

def clientAuthCheck(required=False):
    def f1(fn):
        @wraps(fn)
        def f2(*args, **kwargs):
            g.user = _getUser()
            if not g.user and required:
                abort(401)
            return fn(*args, **kwargs)
        return f2
    return f1

def adminAuthCheck():
    def f1(fn):
        @wraps(fn)
        def f2(*args, **kwargs):
            g.user = _getUser()
            if not g.user and g.user.role != 'admin':
                abort(401)
            return fn(*args, **kwargs)
        return f2
    return f1


def _getUser():
    token = request.headers.get("Authorization")
    userId = checkToken(token, "client")
    if not userId:
        return None
    user = User.query.get(userId)
    if not user or user.token != token:
        return None
    return user




def createToken(userId, role):
    return jwt.encode({
        "id": userId,
        "role": role,
        "exp": datetime.now().timestamp() + 86400 * tokenExpDays
    }, app.config["SECRET_KEY"])


def checkToken(token, role):
    try:
        data = jwt.decode(token, app.config["SECRET_KEY"], algorithms=["HS256"])
        if data["role"] == role:
            return data["id"]
        else:
            return None
    except:
        return None


def getUserData(user):
    resp = UserSchema(only=["id", "email", "name", "token", "role", "last_date_request"]).dump(user)
    return resp

@clientAuthCheck(required=True)
def clientCheckToken():
    return jsonify(getUserData(g.user))

@clientAuthCheck(required=True)
def clientLogout():
    g.user.token = None
    db.session.commit()
    return ""
