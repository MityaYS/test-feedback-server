from datetime import datetime

def timestampNow():
    return datetime.now().timestamp()


def datesIsDifferent(timestamp1, timestamp2):
    date1 = datetime.fromtimestamp(timestamp1)
    date2 = datetime.fromtimestamp(timestamp2)
    return date1.day != date2.day or date1.month != date2.month or date1.year != date2.year
