from datetime import datetime

from flask import jsonify
from models.user import User
from settings import db
from werkzeug.security import generate_password_hash
from config import admins

def setAdmins():
    for admin in admins:
        if not User.query.filter(User.email == admin['email']).first():
            db.session.add(User(
                email=admin['email'],
                name=admin['name'],
                passHash=generate_password_hash(admin['password']),
                role='admin',
                date_register=datetime.now(),
                registerData={"key": None,"isActive": True}
            ))
            db.session.commit()