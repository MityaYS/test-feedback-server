# Installations

1. Python:
   `sudo apt-get update`
   `sudo apt-get install python`

2. PostgreSQL:
   `sudo apt install postgresql postgresql-contrib`

# Database:

1. Create database
   `sudo su - postgres`
   `psql`
   `createdb test_feedback`

2. Server steps
   `python3 -m venv venv` (Install virtual environment - VE)
   `. venv/bin/acitvate` (Enter to VE)
   `python3 -m pip install -r requirements.txt` (Install packages from freezed requirements file)

3. Use Migrate:
   Set password of postgres in config.py and user, if you changed default user
   `flask db init`
   `fask db migrate -m "comment"`
   `fask db upgrade`

4. Last steps:
   In file config.py in array `admins` add your account for admin. After change admins you need restart server.
   `python3 app.py` (Run server)
