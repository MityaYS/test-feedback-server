
from flask import request
from resources.admin.feedback_admin import FeedbackAdminRoute
from settings import app
import manage
from settings import api
from resources.auth.login import clientLogin
from common.auth_utils import clientLogout
from resources.auth.register import clientRegisterStep1, clientRegisterStep2
from common.auth_utils import clientCheckToken
from common.set_default_data import setAdmins
# RESOURCES
from resources.client.feedback_client import FeedbackClientRoute

app.route('/client/login', methods=["POST"])(clientLogin)
# app.route('/client/logout')(clientLogout)
app.route('/register-step-1', methods=["POST"])(clientRegisterStep1)
app.route('/register-step-2')(clientRegisterStep2)
app.route('/client/check-token')(clientCheckToken)

# Client routes
api.add_resource(FeedbackClientRoute, '/feedback')

# Admin routes
api.add_resource(FeedbackAdminRoute, '/admin/feedback')


@app.after_request
def after_request(response):
    if str(request.path).startswith('/static/feedback_files/'):
        response.headers['Content-Disposition'] = 'attachment'
    return response

if __name__ == '__main__':
    setAdmins()
    app.run(debug=True, port=5000, host='localhost')
